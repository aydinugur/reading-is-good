package com.getir.readingisgood.controller;

import com.getir.readingisgood.model.CreateCustomerRequest;
import com.getir.readingisgood.model.CreateCustomerResponse;
import com.getir.readingisgood.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    @Test
    public void testCreateCustomer() {

        Mockito.when(customerService.save(Mockito.any(CreateCustomerRequest.class))).thenReturn("customerId");

        final CreateCustomerRequest request = new CreateCustomerRequest();

        request.setUsername("getıt");
        request.password("toughPassword");

        final CreateCustomerResponse response = customerController.createCustomer(request).getBody();

        Assert.notNull(response, "Customer id is null!");
    }
}
