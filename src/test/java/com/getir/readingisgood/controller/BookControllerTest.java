package com.getir.readingisgood.controller;

import java.math.BigDecimal;

import com.getir.readingisgood.model.CreateBookRequest;
import com.getir.readingisgood.model.CreateBookResponse;
import com.getir.readingisgood.service.BookService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;
import org.springframework.web.client.HttpClientErrorException;

@ExtendWith(MockitoExtension.class)
class BookControllerTest {

    @Mock
    private BookService bookService;

    @InjectMocks
    private BookController bookController;

    private static final double PRICE = 14.99;

    @Test
    public void testCreateBook() {

        Mockito.when(bookService.save(Mockito.any(CreateBookRequest.class))).thenReturn("bookId");

        final CreateBookRequest request = new CreateBookRequest();

        request.setAuthor("FamousAuthor");
        request.setTitle("HowToGetIt");
        request.setPrice(BigDecimal.valueOf(PRICE));

        final CreateBookResponse response = bookController.createBook(request).getBody();

        Assert.notNull(response, "Book id is null!");
    }

    @Test
    public void testCreateBookShouldThrowExceptionWhenAuthorNull() {

        Mockito.when(bookService.save(Mockito.any(CreateBookRequest.class)))
            .thenThrow(HttpClientErrorException.BadRequest.class);

        final CreateBookRequest request = new CreateBookRequest();

        request.setTitle("HowToGetIt");
        request.setPrice(BigDecimal.valueOf(PRICE));

        Assertions.assertThrows(HttpClientErrorException.BadRequest.class, () -> bookController.createBook(request));
    }

}
