package com.getir.readingisgood.service;

import static org.mockito.Mockito.when;

import com.getir.readingisgood.domain.Customer;
import com.getir.readingisgood.model.CreateCustomerRequest;
import com.getir.readingisgood.repository.CustomerRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private CustomerService customerService;

    @Test
    public void testSaveCustomer() {
        final Customer savedCustomer = new Customer();

        savedCustomer.setEnabled(true);
        savedCustomer.setId("customerId");
        savedCustomer.setUsername("soCoolUserName");
        savedCustomer.setPassword("VeryVeryToughPassword");

        when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(savedCustomer);

        when(passwordEncoder.encode(Mockito.any(CharSequence.class))).thenReturn("encryptedPassword");

        final CreateCustomerRequest newCustomer = new CreateCustomerRequest();

        newCustomer.setUsername("soCoolUserName");
        newCustomer.setPassword("VeryVeryToughPassword");

        final String customerId = customerService.save(newCustomer);

        Assert.notNull(customerId, "Customer id is null!");
    }
}
