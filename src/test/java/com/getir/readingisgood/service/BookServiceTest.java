package com.getir.readingisgood.service;

import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import com.getir.readingisgood.domain.Book;
import com.getir.readingisgood.model.CreateBookRequest;
import com.getir.readingisgood.model.UpdateBookStockRequest;
import com.getir.readingisgood.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookService bookService;

    private static final double PRICE = 14.99;
    private static final int STOCK = -10;

    @Test
    public void testSaveBook() {
        final Book savedBook = createBook();

        when(bookRepository.save(Mockito.any(Book.class))).thenReturn(savedBook);

        final String bookId = bookService.save(new CreateBookRequest());

        Assert.notNull(bookId, "Book id is null!");
    }

    @Test
    public void testUpdateBookStock() {
        final Book retrievedBook = createBook();

        when(bookRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(retrievedBook));

        when(bookRepository.save(Mockito.any(Book.class))).thenReturn(retrievedBook);

        final UpdateBookStockRequest updateBookStockRequest = new UpdateBookStockRequest();

        updateBookStockRequest.setStock(STOCK);
        updateBookStockRequest.setId("bookId");

        final String bookId = bookService.updateStock(updateBookStockRequest);

        Assert.notNull(bookId, "Book id is null!");
    }

    private static Book createBook() {
        final Book book = new Book();

        book.setId("bookId");
        book.setAuthor("FamousAuthor");
        book.setTitle("GreatBook");
        book.setAmountSold(0);
        book.setStock(1);
        book.setPrice(BigDecimal.valueOf(PRICE));

        return book;
    }
}
