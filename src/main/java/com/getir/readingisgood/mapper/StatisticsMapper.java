package com.getir.readingisgood.mapper;

import java.math.BigDecimal;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.getir.readingisgood.domain.BookOrder;
import com.getir.readingisgood.domain.Order;
import com.getir.readingisgood.model.GetStatisticsResponse;
import com.getir.readingisgood.model.MonthlyStatistics;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class StatisticsMapper implements Converter<ArrayList<Order>, GetStatisticsResponse> {

    @Override
    public GetStatisticsResponse convert(final MappingContext<ArrayList<Order>, GetStatisticsResponse> context) {
        final List<Order> orders = context.getSource();
        final GetStatisticsResponse response = new GetStatisticsResponse();
        response.setStatistics(new ArrayList<>());

        final Map<Month, List<Order>> monthlyOrders = orders.stream()
            .collect(Collectors.groupingBy(order -> order.getCreatedAt().getMonth()));

        monthlyOrders.forEach((month, orderList) -> {
            final MonthlyStatistics statistics = new MonthlyStatistics();

            statistics.setMonth(month.getDisplayName(TextStyle.SHORT, LocaleContextHolder.getLocale()));

            statistics.setTotalOrderCount(orderList.size());
            statistics.setTotalBookCount(orderList.stream().map(Order::getTotalBookCount).reduce(0, Integer::sum));

            final List<BigDecimal> purchasedAmounts = orderList.stream()
                .map(Order::getBookOrders)
                .map(bookOrders -> bookOrders.stream()
                    .map(BookOrder::getTotalPrice).reduce(BigDecimal.ZERO, BigDecimal::add))
                .collect(Collectors.toList());

            statistics.setTotalPurchasedAmount(purchasedAmounts.stream().reduce(BigDecimal.ZERO, BigDecimal::add));

            response.getStatistics().add(statistics);
        });

        return response;
    }

}
