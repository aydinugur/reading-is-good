package com.getir.readingisgood.exception;

import com.getir.readingisgood.constant.Messages;
import com.getir.readingisgood.model.ErrorResponse;
import com.getir.readingisgood.security.ErrorResponseHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final ErrorResponseHandler errorResponseHandler;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CustomerExistsException.class)
    public ErrorResponse handleCustomerRegistrationExistsException(final CustomerExistsException exception) {
        return getErrorResponseAndLog(
            exception,
            Messages.ERROR_CUSTOMER_EXISTS.getKey(),
            Messages.ERROR_CUSTOMER_EXISTS.getCode()
        );
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BookExistsException.class)
    public ErrorResponse handleBookRegistrationExistsException(final BookExistsException exception) {
        return getErrorResponseAndLog(
            exception,
            Messages.ERROR_BOOK_EXISTS.getKey(),
            Messages.ERROR_BOOK_EXISTS.getCode()
        );
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BookNotExistException.class)
    public ErrorResponse handleBookNotExistException(final BookNotExistException exception) {
        return getErrorResponseAndLog(
            exception,
            Messages.ERROR_BOOK_NOT_EXISTS.getKey(),
            Messages.ERROR_BOOK_NOT_EXISTS.getCode()
        );
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CustomerNotExistException.class)
    public ErrorResponse handleCustomerNotExistException(final CustomerNotExistException exception) {
        return getErrorResponseAndLog(
            exception,
            Messages.ERROR_CUSTOMER_NOT_EXISTS.getKey(),
            Messages.ERROR_CUSTOMER_NOT_EXISTS.getCode()
        );
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NotEnoughStockException.class)
    public ErrorResponse handleNotEnoughStockException(final NotEnoughStockException exception) {
        return getErrorResponseAndLog(
            exception,
            Messages.ERROR_NOT_ENOUGH_STOCK.getKey(),
            Messages.ERROR_NOT_ENOUGH_STOCK.getCode()
        );
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(OrderNotExistException.class)
    public ErrorResponse handleNotOrderNotExitException(final OrderNotExistException exception) {
        return getErrorResponseAndLog(
            exception,
            Messages.ERROR_ORDER_NOT_EXISTS.getKey(),
            Messages.ERROR_ORDER_NOT_EXISTS.getCode()
        );
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidOrderStatusException.class)
    public ErrorResponse handleInvalidOrderStatusException(final InvalidOrderStatusException exception) {
        return getErrorResponseAndLog(
            exception,
            Messages.ERROR_INVALID_ORDER_STATUS.getKey(),
            Messages.ERROR_INVALID_ORDER_STATUS.getCode()
        );
    }

    private ErrorResponse getErrorResponseAndLog(final Exception exception, final String errorMessageKey,
        final int code) {

        log.error(exception.getMessage() + exception);

        return errorResponseHandler.createErrorResponse(errorMessageKey, code);
    }

}
