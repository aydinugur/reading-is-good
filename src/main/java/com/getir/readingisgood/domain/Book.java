package com.getir.readingisgood.domain;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Books")
public class Book {

    @Id
    private String id;

    @Indexed(unique = true)
    @NotNull(message = "Book title must not be null")
    private String title;

    @NotNull(message = "Book author must not be null")
    private String author;

    @NotNull(message = "Book price must not be null")
    @Min(0)
    private BigDecimal price;

    @NotNull(message = "Book stock must not be null")
    @Min(0)
    private Integer stock;

    @NotNull(message = "User's first name must not be null")
    private Integer amountSold;

}
