package com.getir.readingisgood.domain;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "BookOrders")
public class BookOrder {

    @Id
    private String id;

    @DBRef
    private Book book;

    @Min(0)
    private int amount;

    @NotNull
    private BigDecimal totalPrice;

}
