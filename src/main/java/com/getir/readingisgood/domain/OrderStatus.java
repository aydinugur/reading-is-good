package com.getir.readingisgood.domain;

public enum OrderStatus {

    PENDING,
    COMPLETED,
    CANCELLED;

    public static boolean isValid(final OrderStatus currentStatus, final OrderStatus nextStatus) {

        if (currentStatus == PENDING) {
            return nextStatus == COMPLETED || nextStatus == CANCELLED;
        } else if (currentStatus == COMPLETED || currentStatus == CANCELLED) {
            return false;
        } else {
            throw new RuntimeException("Unknown status.");
        }
    }
}
