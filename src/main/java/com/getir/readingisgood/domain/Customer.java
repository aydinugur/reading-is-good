package com.getir.readingisgood.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Customers")
public class Customer {

    @Id
    private String id;

    @Indexed(unique = true)
    private String username;

    private String password;
    private boolean enabled;

}
