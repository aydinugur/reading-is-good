package com.getir.readingisgood.domain;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Orders")
public class Order {

    @Id
    private String id;

    private Customer customer;

    private Set<BookOrder> bookOrders = new HashSet<>();

    private int totalBookCount;

    private OrderStatus status;

    @CreatedDate
    private LocalDate createdAt;
}
