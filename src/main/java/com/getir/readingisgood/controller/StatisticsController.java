package com.getir.readingisgood.controller;

import java.util.List;

import com.getir.readingisgood.api.StatisticApi;
import com.getir.readingisgood.domain.Customer;
import com.getir.readingisgood.domain.Order;
import com.getir.readingisgood.model.GetStatisticsResponse;
import com.getir.readingisgood.service.CustomerService;
import com.getir.readingisgood.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StatisticsController implements StatisticApi {

    private final CustomerService customerService;

    private final OrderService orderService;

    private final ModelMapper mapper;

    @Override
    public ResponseEntity<GetStatisticsResponse> getStatistics() {
        final Customer customer = customerService.getCustomer();

        final List<Order> orders = orderService.getOrders(customer);

        final GetStatisticsResponse response = mapper.map(orders, GetStatisticsResponse.class);

        return ResponseEntity.ok(response);
    }

}
