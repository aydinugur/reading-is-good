package com.getir.readingisgood.controller;

import javax.validation.Valid;

import com.getir.readingisgood.api.BookApi;
import com.getir.readingisgood.model.CreateBookRequest;
import com.getir.readingisgood.model.CreateBookResponse;
import com.getir.readingisgood.model.UpdateBookStockRequest;
import com.getir.readingisgood.model.UpdateBookStockResponse;
import com.getir.readingisgood.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequiredArgsConstructor
public class BookController implements BookApi {

    private final BookService bookService;

    @Override
    public ResponseEntity<CreateBookResponse> createBook(@Valid final CreateBookRequest createBookRequest) {

        final String bookId = bookService.save(createBookRequest);

        final UriComponents uriComponents = UriComponentsBuilder.fromUriString("/book/{id}").buildAndExpand(bookId);

        final CreateBookResponse createBookResponse = new CreateBookResponse();

        createBookResponse.setId(bookId);

        return ResponseEntity.created(uriComponents.toUri()).body(createBookResponse);

    }

    @Override
    public ResponseEntity<UpdateBookStockResponse> updateBookStock(
        @Valid final UpdateBookStockRequest updateBookStockRequest) {

        final String bookId = bookService.updateStock(updateBookStockRequest);

        final UpdateBookStockResponse updateBookStockResponse = new UpdateBookStockResponse();

        updateBookStockResponse.setId(bookId);

        return ResponseEntity.ok(updateBookStockResponse);
    }

}
