package com.getir.readingisgood.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.getir.readingisgood.api.CustomerApi;
import com.getir.readingisgood.domain.BookOrder;
import com.getir.readingisgood.domain.Customer;
import com.getir.readingisgood.domain.Order;
import com.getir.readingisgood.model.CreateCustomerRequest;
import com.getir.readingisgood.model.CreateCustomerResponse;
import com.getir.readingisgood.model.CustomerOrderWithStatus;
import com.getir.readingisgood.model.GetCustomerOrdersResponse;
import com.getir.readingisgood.service.CustomerService;
import com.getir.readingisgood.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequiredArgsConstructor
public class CustomerController implements CustomerApi {

    private final CustomerService customerService;

    private final OrderService orderService;

    @Override
    public ResponseEntity<CreateCustomerResponse> createCustomer(
        @Valid final CreateCustomerRequest createCustomerRequest) {

        final String customerId = customerService.save(createCustomerRequest);

        final CreateCustomerResponse createCustomerResponse = new CreateCustomerResponse();

        createCustomerResponse.setCustomerid(customerId);

        final UriComponents uriComponents = UriComponentsBuilder
            .fromUriString("/customer/{id}").buildAndExpand(customerId);

        return ResponseEntity.created(uriComponents.toUri()).body(createCustomerResponse);
    }

    @Override
    public ResponseEntity<GetCustomerOrdersResponse> getCustomerOrders() {
        final Customer customer = customerService.getCustomer();

        final List<Order> orders = orderService.getOrders(customer);

        final List<CustomerOrderWithStatus> customerOrders = new ArrayList<>();

        for (Order order : orders) {
            for (BookOrder bookOrder : order.getBookOrders()) {
                final CustomerOrderWithStatus customerOrder = new CustomerOrderWithStatus();

                customerOrder.setAuthor(bookOrder.getBook().getAuthor());
                customerOrder.setTitle(bookOrder.getBook().getTitle());
                customerOrder.setAmount(bookOrder.getAmount());
                customerOrder.setStatus(order.getStatus().name());
                customerOrder.setPrice(bookOrder.getTotalPrice());

                customerOrders.add(customerOrder);
            }
        }

        final GetCustomerOrdersResponse response = new GetCustomerOrdersResponse();

        response.setOrders(customerOrders);

        return ResponseEntity.ok(response);
    }

}
