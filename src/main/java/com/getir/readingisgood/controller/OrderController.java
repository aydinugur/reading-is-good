package com.getir.readingisgood.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.getir.readingisgood.api.OrderApi;
import com.getir.readingisgood.domain.BookOrder;
import com.getir.readingisgood.domain.Customer;
import com.getir.readingisgood.domain.Order;
import com.getir.readingisgood.model.CancelOrderRequest;
import com.getir.readingisgood.model.CancelOrderResponse;
import com.getir.readingisgood.model.CompleteOrderRequest;
import com.getir.readingisgood.model.CompleteOrderResponse;
import com.getir.readingisgood.model.CreateOrderRequest;
import com.getir.readingisgood.model.CreateOrderResponse;
import com.getir.readingisgood.model.CustomerOrder;
import com.getir.readingisgood.model.GetOrderResponse;
import com.getir.readingisgood.model.GetOrdersResponse;
import com.getir.readingisgood.model.GetOrdersResponseOrders;
import com.getir.readingisgood.service.CustomerService;
import com.getir.readingisgood.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequiredArgsConstructor
public class OrderController implements OrderApi {

    private final OrderService orderService;
    private final CustomerService customerService;

    @Override
    public ResponseEntity<CreateOrderResponse> orderBook(final CreateOrderRequest createOrderRequest) {

        final Customer customer = customerService.getCustomer();
        final String orderId = orderService.orderBook(createOrderRequest.getOrders(), customer);

        final CreateOrderResponse response = new CreateOrderResponse();

        final UriComponents uriComponents = UriComponentsBuilder.fromUriString("/order/{id}").buildAndExpand(orderId);

        response.setId(orderId);

        return ResponseEntity.created(uriComponents.toUri()).body(response);
    }

    @Override
    public ResponseEntity<GetOrderResponse> getOrder(final String orderId) {
        final Order order = orderService.getOrder(orderId);

        final GetOrderResponse response = new GetOrderResponse();

        response.setCustomer(order.getCustomer().getUsername());
        response.setCreateDate(order.getCreatedAt());
        response.setStatus(order.getStatus().name());
        response.setBooks(new ArrayList<>());

        for (BookOrder bookOrder : order.getBookOrders()) {
            final CustomerOrder customerOrder = new CustomerOrder();

            customerOrder.setAuthor(bookOrder.getBook().getAuthor());
            customerOrder.setTitle(bookOrder.getBook().getTitle());
            customerOrder.setAmount(bookOrder.getAmount());
            customerOrder.setPrice(bookOrder.getTotalPrice());

            response.getBooks().add(customerOrder);
        }

        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<GetOrdersResponse> getOrders(final LocalDate startDate, final LocalDate endDate) {
        final List<Order> orders = orderService.getOrders(startDate, endDate);

        final GetOrdersResponse response = new GetOrdersResponse();

        response.setOrders(new ArrayList<>());

        for (Order order : orders) {
            final GetOrdersResponseOrders getOrdersResponseOrders = new GetOrdersResponseOrders();

            getOrdersResponseOrders.setCustomer(order.getCustomer().getUsername());
            getOrdersResponseOrders.setCreateDate(order.getCreatedAt());
            getOrdersResponseOrders.setStatus(order.getStatus().name());
            getOrdersResponseOrders.setBooks(new ArrayList<>());

            for (BookOrder bookOrder : order.getBookOrders()) {
                final CustomerOrder customerOrder = new CustomerOrder();

                customerOrder.setAuthor(bookOrder.getBook().getAuthor());
                customerOrder.setTitle(bookOrder.getBook().getTitle());
                customerOrder.setAmount(bookOrder.getAmount());
                customerOrder.setPrice(bookOrder.getTotalPrice());

                getOrdersResponseOrders.getBooks().add(customerOrder);
            }

            response.getOrders().add(getOrdersResponseOrders);
        }

        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<CancelOrderResponse> cancelOrder(final CancelOrderRequest cancelOrderRequest) {
        final String orderId = orderService.cancelOrder(cancelOrderRequest.getId());

        final CancelOrderResponse response = new CancelOrderResponse();

        response.setId(orderId);

        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<CompleteOrderResponse> completeOrder(final CompleteOrderRequest completeOrderRequest) {
        final String orderId = orderService.completeOrder(completeOrderRequest.getId());

        final CompleteOrderResponse response = new CompleteOrderResponse();

        response.setId(orderId);

        return ResponseEntity.ok(response);
    }

}
