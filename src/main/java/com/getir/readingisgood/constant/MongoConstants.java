package com.getir.readingisgood.constant;

public final class MongoConstants {

    private MongoConstants() {

    }

    public static final String CREATED_AT = "createdAt";

}
