package com.getir.readingisgood.constant;

public final class PathConstants {

    private PathConstants() {

    }

    public static final String CUSTOMER_REGISTRATION_PATH = "/customer";
    public static final String SWAGGER_UI_PATH = "/swagger-ui.html";
    public static final String SWAGGER_DOCS_PATH = "/v2/api-docs";
    public static final String SWAGGER_CONF_PATH = "/configuration/ui";
    public static final String SWAGGER_RESOURCES_PATH = "/swagger-resources/**";
    public static final String SWAGGER_SECURITY_PATH = "/configuration/security";
    public static final String WEB_JARS_PATH = "/webjars/**";
    public static final String LOGIN_PATH = "/login";
    public static final String LOGOUT_PATH = "/logout";
}
