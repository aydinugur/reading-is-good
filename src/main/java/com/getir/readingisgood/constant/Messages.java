package com.getir.readingisgood.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Messages {

    ERROR_CUSTOMER_EXISTS("error.customer.exists", 1000),
    ERROR_AUTHENTICATION_FAILED("authentication.failed", 1002),
    ERROR_AUTHENTICATION_REQUIRED("authentication.required", 1001),
    ERROR_BOOK_EXISTS("error.book.exists", 1003),
    ERROR_BOOK_NOT_EXISTS("error.book.not.exist", 1004),
    ERROR_CUSTOMER_NOT_EXISTS("error.customer.not.exist", 1005),
    ERROR_NOT_ENOUGH_STOCK("error.not.enough.stock", 1006),
    ERROR_ORDER_NOT_EXISTS("error.order.not.exist", 1007),
    ERROR_INVALID_ORDER_STATUS("error.invalid.order.status", 1008);

    private final String key;
    private final int code;
}
