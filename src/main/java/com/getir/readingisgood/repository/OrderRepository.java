package com.getir.readingisgood.repository;

import java.util.List;

import com.getir.readingisgood.domain.Customer;
import com.getir.readingisgood.domain.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, String> {

    List<Order> findByCustomer(Customer customer);

}
