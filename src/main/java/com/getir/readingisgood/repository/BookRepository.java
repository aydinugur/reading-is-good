package com.getir.readingisgood.repository;

import java.util.List;
import java.util.Optional;

import com.getir.readingisgood.domain.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository<Book, String> {

    Optional<Book> findById(String bookId);

    List<Book> findByIdIn(List<String> bookIds);
}
