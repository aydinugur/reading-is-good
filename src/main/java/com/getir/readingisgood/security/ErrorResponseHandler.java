package com.getir.readingisgood.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.readingisgood.model.ErrorResponse;
import com.getir.readingisgood.utility.MessageReader;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

@Component
@RequiredArgsConstructor
public class ErrorResponseHandler {

    private final ObjectMapper objectMapper;
    private final MessageReader messageReader;

    public void handleError(final HttpServletResponse response, final int status, final String messageKey,
        final int code) throws IOException {

        if (!response.isCommitted()) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding(StandardCharsets.UTF_8.name()); // needed for any non default locale
            response.setStatus(status);

            final PrintWriter writer = response.getWriter();

            objectMapper.writeValue(writer, createErrorResponse(messageKey, code));

            writer.flush();
            writer.close();
        }
    }

    public ErrorResponse createErrorResponse(final String messageKey, final int code) {
        final ErrorResponse error = new ErrorResponse();

        error.setCode(code);
        error.setMessage(messageReader.getMessage(messageKey));

        return error;
    }
}
