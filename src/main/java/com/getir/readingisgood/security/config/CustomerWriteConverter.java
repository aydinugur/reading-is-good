package com.getir.readingisgood.security.config;

import com.getir.readingisgood.domain.Customer;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;

public class CustomerWriteConverter implements Converter<Customer, Document> {

  public Document convert(final Customer customer) {
    Document document = new Document();
    document.put("_id", customer.getId());
    document.put("username", customer.getUsername());
    document.put("password", customer.getPassword());
    document.put("enabled", customer.isEnabled());

    return document;
  }
}
