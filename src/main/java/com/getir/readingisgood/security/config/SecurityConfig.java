package com.getir.readingisgood.security.config;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.readingisgood.constant.Messages;
import com.getir.readingisgood.constant.PathConstants;
import com.getir.readingisgood.security.AppUserDetailsService;
import com.getir.readingisgood.security.RequestBodyReaderAuthenticationFilter;
import com.getir.readingisgood.utility.MessageReader;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final MessageReader messageReader;

    private final AuthenticationEntryPoint authenticationEntryPoint;

    private final PasswordEncoder passwordEncoder;

    private final AppUserDetailsService appUserDetailsService;

    private final ObjectMapper objectMapper;

    @Override
    public void configure(final WebSecurity web) {
        web.ignoring()
            .antMatchers(HttpMethod.POST, PathConstants.CUSTOMER_REGISTRATION_PATH)
            .antMatchers(
                PathConstants.SWAGGER_DOCS_PATH,
                PathConstants.SWAGGER_SECURITY_PATH,
                PathConstants.SWAGGER_RESOURCES_PATH,
                PathConstants.SWAGGER_SECURITY_PATH,
                PathConstants.SWAGGER_UI_PATH,
                PathConstants.WEB_JARS_PATH
            );
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
            .anyRequest().authenticated()
            .and()
            .addFilterBefore(
                authenticationFilter(),
                UsernamePasswordAuthenticationFilter.class
            )
            .logout()
            .logoutUrl(PathConstants.LOGOUT_PATH)
            .logoutSuccessHandler(this::logoutSuccessHandler)
            .and()
            .exceptionHandling()
            .authenticationEntryPoint(authenticationEntryPoint);
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(appUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder);

        return authProvider;
    }

    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authProvider());
    }

    @Bean
    public RequestBodyReaderAuthenticationFilter authenticationFilter() throws Exception {
        final RequestBodyReaderAuthenticationFilter authenticationFilter = new RequestBodyReaderAuthenticationFilter();

        authenticationFilter.setAuthenticationSuccessHandler(this::loginSuccessHandler);
        authenticationFilter.setAuthenticationFailureHandler(this::loginFailureHandler);

        authenticationFilter.setRequiresAuthenticationRequestMatcher(
            new AntPathRequestMatcher(PathConstants.LOGIN_PATH, HttpMethod.POST.name())
        );

        authenticationFilter.setAuthenticationManager(authenticationManagerBean());

        return authenticationFilter;
    }

    private void loginSuccessHandler(
        final HttpServletRequest request,
        final HttpServletResponse response,
        final Authentication authentication) throws IOException {

        response.setStatus(HttpStatus.OK.value());
        objectMapper.writeValue(response.getWriter(), messageReader);
    }

    private void loginFailureHandler(
        final HttpServletRequest request,
        final HttpServletResponse response,
        final AuthenticationException e) throws IOException {

        response.setStatus(HttpStatus.UNAUTHORIZED.value());

        objectMapper
            .writeValue(response.getWriter(), messageReader.getMessage(Messages.ERROR_AUTHENTICATION_FAILED.getKey()));
    }

    private void logoutSuccessHandler(
        final HttpServletRequest request,
        final HttpServletResponse response,
        final Authentication authentication) throws IOException {

        response.setStatus(HttpStatus.OK.value());
        objectMapper.writeValue(response.getWriter(), "We will miss you!");
    }

}
