package com.getir.readingisgood.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.getir.readingisgood.constant.Messages;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ApiAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final ErrorResponseHandler errorResponseHandler;

    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response,
                         final AuthenticationException exception) throws IOException {

        log.error("Exception occurred:{} exception: {}", exception.getMessage(), exception);

        errorResponseHandler.handleError(
            response,
            HttpServletResponse.SC_UNAUTHORIZED,
            Messages.ERROR_AUTHENTICATION_REQUIRED.getKey(),
            Messages.ERROR_AUTHENTICATION_REQUIRED.getCode()
        );

    }
}
