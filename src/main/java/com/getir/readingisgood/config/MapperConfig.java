package com.getir.readingisgood.config;

import com.getir.readingisgood.mapper.StatisticsMapper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class MapperConfig {

    private final StatisticsMapper statisticsMapper;

    @Bean
    public ModelMapper modelMapper() {
        final ModelMapper mapper = new ModelMapper();

        mapper.addConverter(statisticsMapper);

        return mapper;
    }
}
