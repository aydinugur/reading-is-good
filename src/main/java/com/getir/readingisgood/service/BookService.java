package com.getir.readingisgood.service;

import java.util.List;

import com.getir.readingisgood.domain.Book;
import com.getir.readingisgood.exception.BookExistsException;
import com.getir.readingisgood.exception.BookNotExistException;
import com.getir.readingisgood.exception.GeneralSystemException;
import com.getir.readingisgood.model.CreateBookRequest;
import com.getir.readingisgood.model.UpdateBookStockRequest;
import com.getir.readingisgood.repository.BookRepository;
import com.mongodb.MongoWriteException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookService {

    private final BookRepository bookRepository;

    @Transactional
    public String save(final CreateBookRequest createBookRequest) {
        final Book book = new Book();

        book.setTitle(createBookRequest.getTitle());
        book.setAuthor(createBookRequest.getAuthor());
        book.setPrice(createBookRequest.getPrice());
        book.setStock(1);
        book.setAmountSold(0);

        String bookId;

        try {
            bookId = bookRepository.save(book).getId();

            log.info("Book Id with {} created", bookId);
        } catch (final DataIntegrityViolationException exception) {
            log.debug("Error occurred ex: {}", exception.getCause());

            if (exception.getCause() instanceof MongoWriteException) {
                throw new BookExistsException();
            }
            throw new GeneralSystemException();
        }

        return bookId;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public String updateStock(final UpdateBookStockRequest updateBookStockRequest) {
        final Book book = retrieveBook(updateBookStockRequest.getId());

        final int newStock = Math.max(0, book.getStock() + updateBookStockRequest.getStock());

        if (newStock == 0) {
            log.debug("Book id {} stock has been set to zero!", book.getId());
        }

        book.setStock(newStock);

        return bookRepository.save(book).getId();
    }

    @Transactional(readOnly = true)
    public Book retrieveBook(final String bookId) {
        return bookRepository.findById(bookId).orElseThrow(BookNotExistException::new);
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<Book> retrieveBooks(final List<String> bookIds) {
        return bookRepository.findByIdIn(bookIds);
    }
}
