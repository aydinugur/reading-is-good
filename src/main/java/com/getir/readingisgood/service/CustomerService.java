package com.getir.readingisgood.service;

import java.util.Objects;

import com.getir.readingisgood.exception.CustomerExistsException;
import com.getir.readingisgood.exception.CustomerNotExistException;
import com.getir.readingisgood.exception.GeneralSystemException;
import com.getir.readingisgood.model.CreateCustomerRequest;
import com.getir.readingisgood.domain.Customer;
import com.getir.readingisgood.repository.CustomerRepository;
import com.getir.readingisgood.security.UserDetailsImpl;
import com.mongodb.MongoWriteException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
@Slf4j
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public String save(final CreateCustomerRequest createCustomerRequest) throws DataIntegrityViolationException {

        final Customer customer = new Customer();

        customer.setUsername(createCustomerRequest.getUsername());
        customer.setPassword(passwordEncoder.encode(createCustomerRequest.getPassword()));
        customer.setEnabled(true);

        String customerId;

        try {
            customerId = customerRepository.save(customer).getId();

            log.info("Customer with id {} created", customerId);
        } catch (final DataIntegrityViolationException exception) {
            log.debug("Error occurred ex: {}", exception.getCause());

            if (exception.getCause() instanceof MongoWriteException) {
                throw new CustomerExistsException();
            }
            throw new GeneralSystemException();
        }

        return customerId;
    }

    public Customer getCustomer() {

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (Objects.nonNull(authentication)) {
            final UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            return userDetails.getCustomer();
        }

        throw new CustomerNotExistException();
    }
}
