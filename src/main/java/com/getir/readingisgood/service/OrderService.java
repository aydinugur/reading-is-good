package com.getir.readingisgood.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getir.readingisgood.constant.MongoConstants;
import com.getir.readingisgood.domain.Book;
import com.getir.readingisgood.domain.BookOrder;
import com.getir.readingisgood.domain.Customer;
import com.getir.readingisgood.domain.Order;
import com.getir.readingisgood.domain.OrderStatus;
import com.getir.readingisgood.exception.InvalidOrderStatusException;
import com.getir.readingisgood.exception.NotEnoughStockException;
import com.getir.readingisgood.exception.OrderNotExistException;
import com.getir.readingisgood.model.OrderModel;
import com.getir.readingisgood.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    private final BookService bookService;

    private final MongoOperations mongoOperations;

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public String orderBook(final List<OrderModel> orders, final Customer customer) {
        final Map<String, Integer> bookOrders = new HashMap<>();

        orders.forEach(order -> {
            int amount = bookOrders.getOrDefault(order.getBookId(), 0) + order.getAmount();

            bookOrders.put(order.getBookId(), amount);
        });

        final List<Book> books = bookService.retrieveBooks(new LinkedList<>(bookOrders.keySet()));

        int totalAmount = 0;

        final Order order = new Order();

        for (final Book book : books) {

            final int availableStock = book.getStock();
            final int orderedBookAmount = bookOrders.get(book.getId());

            if (availableStock >= orderedBookAmount) {
                book.setStock(availableStock - orderedBookAmount);
                book.setAmountSold(book.getAmountSold() + orderedBookAmount);
                totalAmount += orderedBookAmount;

                final BookOrder bookOrder = new BookOrder();

                bookOrder.setBook(book);
                bookOrder.setAmount(orderedBookAmount);
                bookOrder.setTotalPrice(book.getPrice().multiply(BigDecimal.valueOf(orderedBookAmount)));

                order.getBookOrders().add(bookOrder);
                order.setStatus(OrderStatus.PENDING);
                order.setCustomer(customer);
            } else {
                throw new NotEnoughStockException();
            }
        }

        order.setTotalBookCount(totalAmount);
        order.setCreatedAt(LocalDate.now());

        return orderRepository.save(order).getId();
    }

    @Transactional(readOnly = true)
    public Order getOrder(final String orderId) {
        return orderRepository.findById(orderId).orElseThrow(OrderNotExistException::new);
    }

    @Transactional(readOnly = true)
    public List<Order> getOrders(final LocalDate startDate, final LocalDate endDate) {
        Query query = new Query(
            Criteria.where(MongoConstants.CREATED_AT).gte(startDate)
                .andOperator(Criteria.where(MongoConstants.CREATED_AT).lt(endDate))
        );

        return mongoOperations.find(query, Order.class);
    }

    @Transactional(readOnly = true)
    public List<Order> getOrders(final Customer customer) {
        return orderRepository.findByCustomer(customer);
    }

    @Transactional
    public String completeOrder(final String id) {
        final Order order = orderRepository.findById(id).orElseThrow(OrderNotExistException::new);

        if (OrderStatus.isValid(order.getStatus(), OrderStatus.COMPLETED)) {
            order.setStatus(OrderStatus.COMPLETED);

            return orderRepository.save(order).getId();
        }

        throw new InvalidOrderStatusException();
    }

    @Transactional
    public String cancelOrder(final String id) {
        final Order order = orderRepository.findById(id).orElseThrow(OrderNotExistException::new);

        if (OrderStatus.isValid(order.getStatus(), OrderStatus.CANCELLED)) {
            final Set<BookOrder> bookOrders = order.getBookOrders();

            for (BookOrder bookOrder : bookOrders) {
                final Integer returnAmount = bookOrder.getAmount();

                final Book book = bookOrder.getBook();

                book.setStock(book.getStock() + returnAmount);
                book.setAmountSold(book.getAmountSold() + returnAmount);
            }

            order.setStatus(OrderStatus.CANCELLED);

            return orderRepository.save(order).getId();
        }
        throw new InvalidOrderStatusException();
    }
}
