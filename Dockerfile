FROM openjdk:11.0.1-jre-slim

MAINTAINER Ugur Aydin <aydinugur@yahoo.com>

ARG JAR_FILE=build/libs/reading-is-good-*.jar

ADD ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","/app.jar"]

EXPOSE 8080