# Reading is good
This project dedicated to Ugur's hiring process for Getir company.

## Tech stack
* Java 11
* Gradle 7.2v
* Spring-boot 2.5.5v
* Open-api 5.2.1v
* Mongodb
* Mongo Express
* Lombok
* Checkstyle

## Run
The project has Mongodb as a database
```
./gradle.sh build
docker compose up
```

Docker compose provides mongodb connection and ui (Mongo Express) too

It runs the application on port 8080, Mongodb on 27017 and Mongo Express on 8081

To see Mongo db documents and modify see localhost:8081

To see API documentation see localhost:8080/swagger-ui.html

There is a postman collection ready to import in $root/postman/reading_is_good.postman_collection.json
